module Battlerite

  class Asset < Battlerite::Model
    attr_reader :created, :description, :name, :url, :blob

    def self.munch asset, relationships, blob
      a = self.new(
        id: asset["id"],
        created: Time.parse(asset["attributes"]["createdAt"]),
        description: asset["attributes"]["description"],
        name: asset["attributes"]["name"],
        url: asset["attributes"]["URL"],
        relationships: relationships,
        blob: blob,
      )
    end

  end

end
