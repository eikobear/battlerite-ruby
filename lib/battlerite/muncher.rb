require_relative "model"

module Battlerite
  class Muncher

    def self.munch blob
      data = blob["data"].respond_to?(:to_ary) ?  blob["data"] : [blob["data"]]
      data.each_with_object([]) do |datum, results|
        results << self.munch_agnostic(datum, blob)
      end
    end

    def self.munch_agnostic record, blob
      relationships = Hash.new { |h, k| h[k] = [] }
      record["relationships"].each do |rel, value|
        data = value["data"].nil? ? [] : value["data"]
        data = data.respond_to?(:to_ary) ? data : [data]
        relationships[rel.to_sym] += data.map { |datum| munch_included(datum["id"], blob) }
      end unless record["relationships"].nil?

      Battlerite.const_get(record["type"].capitalize).munch record, relationships, blob
    end

    def self.munch_included id, blob
      findings = blob["included"].find_all { |item| item["id"] == id }
      raise DuplicateRecordError.new id: id, records: findings if findings.count > 1
      self.munch_agnostic findings[0], blob
    end

  end
end
