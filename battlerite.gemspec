Gem::Specification.new do |s|
  s.name = "battlerite"
  s.version = "1.0.0"
  s.date = "2017-12-04"
  s.summary = "API wrapper for Battlerite"
  s.description = "API wrapper for Battlerite, with convenient but flexible wrapper Models."
  s.authors = ["eiko kokuma"]
  s.email = "kokumeiko@gmail.com"
  s.files = ["lib/battlerite.rb"] + Dir["lib/**/*.rb"]
  s.homepage = "https://gitlab.com/eiko/battlerite-ruby"
  s.license = "MIT"
end
