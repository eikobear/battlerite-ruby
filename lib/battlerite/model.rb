module Battlerite

  class Battlerite::Model

    @@dig = true

    def self.enable_dynamic_dig enabled
      @@dig = enabled
    end

    def self.alias_attribute new, old
      @aliases ||= {}
      @aliases[new] = old
    end
    
    def self.aliases
      @aliases ||= {}
    end

    attr_reader :id, :relationships, :blob

    def initialize params
      @relationships = params.delete :relationships
      @blob = params.delete :blob

      params.each { |k, v| self.instance_variable_set :"@#{k}", v }

      @relationships.each { |k, v| self.define_singleton_method(k.to_sym) { v } }

      self.class.aliases.each { |new, old| self.define_singleton_method(new.to_sym) { |*args, &block| self.public_send(old, *args, &block) } }
    end

    def method_missing method, *args, &block
      return dig(method, *args, &block) unless dig_path(method).empty? || @@dig == false
      super(method, *args, &block)
    end

    def respond_to? method, *args, &block
      return !dig_path(method).empty? || super(method, *args, &block) if @@dig
      return super(method, *args, &block)
    end

    def method(method)
      if !dig_path(method).empty? && @@dig
        super(:dig)
      else
        super(method)
      end
    end

    def dig_path(method)
      return [self] if @relationships.keys.include?(method)
      return [self] if self.class.aliases.keys.include?(method)
      return @relationships.values.map { |v| v.map { |i| i.dig_path(method) } }.compact.flatten
    end

    def dig(method, *args, &block)
      dig_path(method).map { |v| v.public_send(method, *args, &block) }.flatten
    end

    def to_s
      "#{self.class.name.split("::").last}: #{id[0,5]}..."
    end

    def to_s_tree
      str = "#{to_s}"
      relationships.each { |k, v| v.each { |d| str += "\n├────#{d.to_s_tree.gsub("\n", "\n│    ")}" } } if respond_to? :relationships
      str
    end

  end


end

Dir[File.join(File.dirname(__FILE__), "models", "*.rb")].each { |file| require file }
