module Battlerite

  class Participant < Battlerite::Model

    attr_reader :actor, :ability_uses, :attachment, :damage_done, :damage_received, :deaths, :disables_done, :disables_received, :emote, :energy_gained, :energy_used, :healing_done, :healing_received 
    attr_reader :kills, :mount, :outfit, :score, :side, :time_alive, :user_id, :relationships, :blob
    alias_attribute :players, :player

    def self.munch participant, relationships, blob
      attr = participant["attributes"]
      stats = attr["stats"]
      p = self.new(
        id: participant["id"],
        actor: attr["actor"],
        ability_uses: stats["abilityUses"],
        attachment: stats["attachment"],
        damage_done: stats["damageDone"],
        damage_received: stats["damageReceived"],
        deaths: stats["deaths"],
        disables_done: stats["disablesDone"],
        disables_received: stats["disablesReceived"],
        emote: stats["emote"],
        energy_gained: stats["energyGained"],
        energy_used: stats["energyUsed"],
        healing_done: stats["healingDone"],
        healing_received: stats["healingReceived"],
        kills: stats["kills"],
        mount: stats["mount"],
        outfit: stats["outfit"],
        score: stats["score"],
        side: stats["side"],
        time_alive: stats["timeAlive"],
        user_id: stats["userID"],
        relationships: relationships,
        blob: blob,
      )
    end

  end

end
