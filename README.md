[![Gem Version](https://badge.fury.io/rb/battlerite.svg)](https://badge.fury.io/rb/battlerite)

# battlerite-ruby API wrapper

This gem wraps the Battlerite gamedata API calls and parses / links the data into structured, accessible objects. 

## Getting Started

battlerite-ruby is installed in the usual ways:
```
gem install battlerite
```

or
```ruby
# Gemfile
gem "battlerite"
```

The Battlerite API requires an API key for authentication. Get your key by signing up at [developer.battlerite.com](https://developer.battlerite.com/). 

battlerite-ruby looks at the `BATTLERITE_API_KEY` ENV variable for the key. Set it however you prefer. I personally like to use the [dotenv](https://github.com/bkeepers/dotenv) gem.

Once configured, simply require the gem and get cooking:

```ruby
require "battlerite"

battlerite = Battlerite.new
matches = br.get_matches
puts "retrieved #{matches.count} matches! yay!"
```

## Requesting Data

Get a list of matches simply by calling `get_matches`. The below example includes all options supported by the Battlerite API; however, you can omit any or all options, and defaults will be used instead. Check [the Battlerite API docs](http://battlerite-docs.readthedocs.io/en/master/matches/matches.html) for defaults. 

WARNING: your mileage may vary for `:game_modes`. I can't tell whether it's implemented or not because the actual names for game modes are not yet available.

```ruby
br = Battlerite.new
matches = br.get_matches(
page_offset: 2, # which page to start on
page_limit: 5, # how many pages to grab (max is 5)
sort: "-createdAt" # sort by match date (- for reverse order)
created_at_start: Time.now - (60 * 60) # find matches from 60 minutes ago
created_at_end: Time.now # to now
game_mode: "some_game_mode" # WARNING: searching by game mode might not work!
player_ids: [123, 456] # filter by player IDs
)
```

battlerite-ruby supports all API endpoints, following the above syntax. 

## Working with Models

battlerite-ruby wraps all API data in convenient models:

```ruby
match = br.get_matches.first
player_id = match.rosters.first.participants.first.players.first.id
```

Unfortunately, the actual Battlerite API structure is not so convenient. You can wrap your head around it using `Battlerite.visualize`:

```ruby
Battlerite.visualize br.get_matches(page_limit: 2)
#Match: AB9C8...
#├────Asset: 18861...
#├────Roster: eb771...
#│    ├────Participant: 7560e...
#│    │    ├────Player: 78308...
#│    ├────Participant: 075d9...
#│    │    ├────Player: 78587...
#├────Roster: 08cd7...
#│    ├────Participant: fcb9e...
#│    │    ├────Player: 78605...
#│    ├────Participant: d7483...
#│    │    ├────Player: 80479...
#├────Round: 5ab18...
#├────Round: c4935...
#├────Round: be463...
#Match: 0D3BB...
#├────Asset: 19960...
#├────Roster: 234fa...
#│    ├────Participant: bdf72...
#│    │    ├────Player: 78074...
#│    ├────Participant: ae217...
#│    │    ├────Player: 79065...
#├────Roster: f37de...
#│    ├────Participant: 948e5...
#│    │    ├────Player: 93340...
#│    ├────Participant: 39b06...
#│    │    ├────Player: 93341...
#├────Round: 53387...
#├────Round: 129fa...
#├────Round: adfe6...
#=>
```

Each Match has 3 or more rounds, as well as 2 Rosters. Each Roster has 2 or 3 Participants, and each Participant represents one Player.

To make this structure more manageable, battlerite-ruby includes syntactic sugar for dynamically digging into a model heirarchy:

```ruby
match.rosters.first.participants.first.players.first.id
#=> "803376223907618816"
match.players.first.id #dig for and collect every player in match
#=> "803376223907618816"
```

Dynamic digging relies on a recursive search of all child Models. `method_missing`, `respond_to?`, and `method` have all been overridden to improve compatibility and ease of use in `irb` or `pry`; however, if you really dislike this sort of thing, you can turn the dynamic portion off:

```ruby
Battlerite::Model.enable_dynamic_dig false
match.players
#=> NoMethodError: undefined method `players` for #<Battlerite::Match>
match.dig(:players)
#=> [#<Battlerite::Player>, ..]

```

## Working without Models

The `Battlerite` class is a simple wrapper which makes api calls using `Battlerite::Api` then parses them into Models. If you want the joy of JSON back in your life, you can use `Battlerite::Api` just like you would `Battlerite`:

```ruby
br = Battlerite::Api.new
br.get_matches page_limit: 2
#=> {"data"=>[{"type"="match", "id"="ABCDEFGHIJGSADLSAJ" ...
```

If you ever need to make a raw API call, you can get down to bare metal with `Battlerite::Api#get`:

```ruby
br = Battlerite::Api.new
uri = URI("https://api.dc01.gamelockerapp.com/shards/global/matches")
br.get uri 
#=> {"data"=>[{"type"="match", "id"="ABCDEFGHIJGSADLSAJ" ...
```

This super thin wrapper only adds the API key for authorization and parses the JSON, but leaves the rest up to you.
