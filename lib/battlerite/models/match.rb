module Battlerite

  class Match < Battlerite::Model

    attr_reader :started, :duration, :finished, :game_mode, :map, :match_type, :relationships, :blob

    def self.munch match, relationships, blob
      started = Time.parse match["attributes"]["createdAt"]
      duration = match["attributes"]["duration"]
      finished = started + duration
      m = self.new(
        id: match["id"],
        started: started,
        duration: duration,
        finished: finished,
        game_mode: match["attributes"]["gameMode"],
        map: match["attributes"]["stats"]["mapID"],
        match_type: match["attributes"]["stats"]["type"],
        relationships: relationships,
        blob: blob,
      )
    end

  end

end
