
module Battlerite

  class DuplicateRecordError < StandardError

    attr_reader :id, :records

    def initialize data, msg="Two or more records exist with the same ID."
      @id = data[:id]
      @records = data[:records]
      @message = msg
    end

    def message
      @message
    end

  end

  class ApiError < StandardError

    attr_reader :res

    def initialize res
      @res = res
    end

    def message
      "Error code #{@res.code} received from Battlerite API. Response body: #{@res.body}"
    end

  end

  class UnauthorizedError < ApiError

    def message
      "Unauthorized (#{@res.code}) error from Battlerite API. Did you set ENV[\"BATTLERITE_API_KEY\"]?"
    end

  end


end

