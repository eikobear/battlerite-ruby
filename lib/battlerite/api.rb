require_relative "errors"

module Battlerite
  class Api

    def uris
      @@uris ||= {
        matches: "https://api.dc01.gamelockerapp.com/shards/global/matches",
        match: "https://api.dc01.gamelockerapp.com/shards/global/matches/:match_id",
        players: "https://api.dc01.gamelockerapp.com/shards/global/players",
        player: "https://api.dc01.gamelockerapp.com/shards/na/players/:player_id",
      }
    end

    def get_matches opt={}
      uri = URI(uris[:matches])
      a = uri.query.nil? ? [] : URI.decode_www_form(uri.query)
      a << ["page[offset]", opt[:page_offset] || 0]
      a << ["page[limit]", opt[:page_limit] || 5]
      a << ["sort", opt[:sort] || "createdAt"]
      a << ["filter[createdAt-start]", opt[:created_at_start] || (Time.now - (28 * 60 * 60 * 24))]
      a << ["filter[createdAt-end]", opt[:created_at_end] || (Time.now)]
      a << ["filter[gameMode]", opt[:game_mode]] if opt[:game_mode]
      a << ["filter[playerIds]", opt[:player_ids]] if opt[:player_ids]
      if opt[:player_names]
        raise NotImplementedError, "Battlerite API 1.0 has not implemented player data yet. Check http://battlerite-docs.readthedocs.io for more information."
      end
      if opt[:team_names]
        raise NotImplementedError, "Battlerite API 1.0 has not implemented team data yet. Check http://battlerite-docs.readthedocs.io for more information."
      end
      #a << ["filter[playerNames]", opt[:player_names]] if opt[:player_names]
      #a << ["filter[teamNames]", opt[:team_names]] if opt[:team_names]
      uri.query = URI.encode_www_form(a)
      get uri
    end

    def get_matches_options opt
      a = []
      a << ["page[offset]", opt[:offset] || 0]
      a << ["page[limit]", opt[:limit] || 5]
      a << ["sort", opt[:sort] || "createdAt"]
      a << ["filter[createdAt-start]", opt[:start_date] || (Time.now - (28 * 60 * 60 * 24))]
      a << ["filter[createdAt-end]", opt[:end_date] || (Time.now - (28 * 60 * 60 * 24))]
      a << ["filter[gameMode]", opt[:game_mode]] if opt[:game_mode]
      a << ["filter[teamNames]", opt[:team_names]] if opt[:team_names]
      a
    end

    def get_match id
      uri = URI(uris[:match].gsub(":match_id", id))
      get uri
    end

    def get_players opt={} 
      uri = URI(uris[:players])
      a = uri.query.nil? ? [] : URI.decode_www_form(uri.query)
      a << ["filter[playerNames]", opt[:player_names]] unless opt[:player_names].nil?
      a << ["filter[playerIds]", opt[:player_ids]] unless opt[:player_ids].nil?
      uri.query = URI.encode_www_form(a)
      get uri
    end

    def get_player id
      uri = URI(uris[:player].gsub(":player_id", id))
      get uri
    end


    def get uri
      req = Net::HTTP::Get.new(uri)
      req["Authorization"] = api_key
      req["Accept"] = "application/vnd.api+json"

      res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do  |http|
        http.request(req)
      end

      raise Battlerite::UnauthorizedError.new(res) if res.code.to_i == 401
      raise Battlerite::ApiError.new(res) if res.code.to_i >= 400

      JSON.parse(res.body)
    end

    def api_key
      ENV["BATTLERITE_API_KEY"]
    end

  end
end
