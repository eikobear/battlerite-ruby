module Battlerite

  class Round < Battlerite::Model
    attr_reader :duration, :ordinal, :winning_team
    alias_method :number, :ordinal

    def self.munch round, relationships, blob
      r = self.new(
        id: round["id"],
        duration: round["attributes"]["duration"],
        ordinal: round["attributes"]["ordinal"],
        winning_team: round["attributes"]["stats"]["winningTeam"],
        relationships: relationships,
        blob: blob,
      )
    end

  end

end
