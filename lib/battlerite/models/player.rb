module Battlerite

  class Player < Battlerite::Model

    attr_reader :name, :stats, :relationships, :blob

    def self.munch player, relationships, blob
      p = self.new(
        id: player["id"],
        name: player["attributes"]["name"],
        stats: player["attributes"]["stats"],
        relationships: relationships,
        blob: blob
      )
    end

    def name
      raise NotImplementedError, "Battlerite API 1.0 has not implemented player data yet. Check http://battlerite-docs.readthedocs.io for more information."
    end

    def stats
      raise NotImplementedError, "Battlerite API 1.0 has not implemented player data yet. Check http://battlerite-docs.readthedocs.io for more information."
    end

  end

end
