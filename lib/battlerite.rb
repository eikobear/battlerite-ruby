require "net/http"
require "json"
require "time"

require_relative "battlerite/api"
require_relative "battlerite/muncher"

module Battlerite

  def self.new
    Instance.new
  end

  def self.visualize models
    models = [models] unless models.respond_to? :to_ary
    puts models.map { |model| model.to_s_tree }
  end

  class Instance

    attr_reader :api

    def initialize
      @api = Api.new

      @api.public_methods(false).each do |method|
        define_singleton_method method do |*args|
          result = @api.send(method, *args)
          Muncher.munch @api.send(method, *args)
        end
      end
    end
    
  end

end


