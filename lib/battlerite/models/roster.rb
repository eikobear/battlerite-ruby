module Battlerite

  class Roster < Battlerite::Model

    attr_reader :score, :won, :relationships, :blob
    alias_method :won?, :won

    def self.munch roster, relationships, blob
      r = self.new(
        id: roster["id"],
        score: roster["attributes"]["stats"]["score"],
        won: roster["attributes"]["stats"]["won"] == "true",
        relationships: relationships,
        blob: blob,
      )
    end

  end

end
